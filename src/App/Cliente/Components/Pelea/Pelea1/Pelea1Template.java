package src.App.Cliente.Components.Pelea.Pelea1;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Image;

import src.App.Services.GraphicServices.ObjGraficService;
import src.App.Services.GraphicServices.RecursosService;

import java.awt.Color;
import java.awt.Font;

public class Pelea1Template extends JPanel {
    private ObjGraficService sObjGrafics;
    private RecursosService sRecursos;
    private ImageIcon iDim;
    private JLabel lab, lab1, lab2, lab3, lab4;
    private JButton bAtaque;
    private int iYo = 100, ijefe = 100;
    private Pelea1Component pelea1Component;
    private static final long serialVersionUID = 1L;

    public Pelea1Template(Pelea1Component pelea1Component) {
        this.pelea1Component = pelea1Component;
        sObjGrafics = ObjGraficService.getService();
        sRecursos = RecursosService.getService();
        crearBotones();
        crearLabel();
        this.setSize(1500, 680);
        this.setBackground(Color.yellow);
        this.setLayout(null);
        this.setVisible(true);
    }
    public void setVidaJefe(int ijefe) {
        this.ijefe = ijefe;
        System.out.println(ijefe);
    }
    public int getVidaJefe() {
        return ijefe;
    } 
    public void crearLabel() {
        //label vida mago
        lab3= sObjGrafics.construirJLabel("vida: "+iYo,360,10, 100, 100,
        null, null, new Font("arial",Font.BOLD,16), null, Color.WHITE, null, "");
        this.add(lab3);
        //label vida jefe
        lab4= sObjGrafics.construirJLabel("vida: "+ijefe,1025,10, 100, 100,
        null, null, new Font("arial",Font.BOLD,16), null, Color.WHITE, null, "");
        this.add(lab4);
        // redimencionar mago yo
        iDim= new ImageIcon(sRecursos.getImagenyo().getImage().getScaledInstance(200, 248, Image.SCALE_AREA_AVERAGING));
       //label 
        lab1= sObjGrafics.construirJLabel("",330,245, 200, 248,
        null, iDim, null, null, null, null, "");
        this.add(lab1);
        // redimencionar jefe
        iDim= new ImageIcon(sRecursos.getImagenMago1().getImage().getScaledInstance(200, 248, Image.SCALE_AREA_AVERAGING));
       //label 
        lab2= sObjGrafics.construirJLabel("",910,245, 200, 248,
        null, iDim, null, null, null, null, "");
        this.add(lab2);
        // redimencionar fondo lava
        iDim= new ImageIcon(sRecursos.getImagenFondoPelea1().getImage().getScaledInstance(1500, 680, Image.SCALE_AREA_AVERAGING));
       //label 
        lab= sObjGrafics.construirJLabel("",0,0, 1500, 680,
        null, iDim, null, null, null, null, "");
        this.add(lab);  
    }
    public void crearBotones(){
        //boton inicio de sesión
        bAtaque= sObjGrafics.construirJButton("Ataque", 250,600,300, 50, 
        sRecursos.getCursor(), null, new Font("arial",Font.BOLD,16), sRecursos.getColorAzul(), Color.white, null, "", true);
        bAtaque.addActionListener(pelea1Component);
        this.add(bAtaque);
    }
    public JLabel getLabel3() {return lab4;}
    
    
}